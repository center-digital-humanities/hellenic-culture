				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Academic Programs subpage
								if (is_tree(863) || get_field('menu_select') == "programs") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Academic Programs', 'bonestheme' ),
										'menu_class' => 'programs-nav',
										'theme_location' => 'programs-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Academic Programs</h3> <ul>%3$s</ul>'
									));
								}
								// If an people subpage
								if (is_tree(462) || get_field('menu_select') == "people") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'People', 'bonestheme' ),
									   	'menu_class' => 'people-nav',
									   	'theme_location' => 'people-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>People</h3> <ul>%3$s</ul>'
									));
								}
								// If an Course subpage
								if (is_tree(178) || get_field('menu_select') == "courses") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Courses', 'bonestheme' ),
									   	'menu_class' => 'courses-nav',
									   	'theme_location' => 'courses-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Courses</h3> <ul>%3$s</ul>'
									));
								}
								// If an affiliated organization subpage
								if (is_tree(216) || get_field('menu_select') == "affiliated") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Affiliated', 'bonestheme' ),
									   	'menu_class' => 'affiliated-nav',
									   	'theme_location' => 'affiliated-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Affiliated Organizations</h3> <ul>%3$s</ul>'
									));
								}
								// If an donors subpage
								if (is_tree(860) || get_field('menu_select') == "donors") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Donors', 'bonestheme' ),
									   	'menu_class' => 'donors-nav',
									   	'theme_location' => 'donors-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Donors</h3> <ul>%3$s</ul>'
									));
								}
								// If an giving subpage
								if (is_tree(219) || get_field('menu_select') == "giving") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Giving', 'bonestheme' ),
									   	'menu_class' => 'giving-nav',
									   	'theme_location' => 'giving-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Giving</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
					</div>
				</div>
				<?php } ?>