<?php
/*
 Template Name: People Listing
*/
?>
<?php get_header(); ?>

    <?php // Select what people category to show
        $people_category = get_field('people_category');
        if( $people_category ) {
            $people_cat = $people_category->slug;
        }
        // Set varaibles to decide behavior of page
        if ( get_field('link_to_pages') == 'yes' ) {
            $person_link = 'yes';
        }
        $people_details = get_field('people_details');
        if( in_array('position', $people_details) ) { 
            $position = 'yes';
        } 
        if( in_array('interest', $people_details) ) {
            $interest = 'yes';
        } 
        if( in_array('email', $people_details) ) {
            $email = 'yes';
        }
        if( in_array('phone', $people_details) ) {
            $phone = 'yes';
        }
        if( in_array('office', $people_details) ) {
            $office = 'yes';
        } 
    ?>
			<div class="content main <?php if($people_cat == "alumni"){ ?>alumni<?php }?>" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
					<?php if ( get_field('display_field_of_study') == 'show' ) { ?>
                        <span class="filter-title">All</span>
                    <?php } ?>
					<?php the_content(); ?>
					<?php if ( get_field('display_field_of_study') == 'show' ) { ?>
					<?php if ( has_nav_menu('faculty-filter') ) { ?> 
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
						<?php if(get_field('filter_label')) { ?>
							<h3><?php the_field('filter_label'); ?></h3>
						<?php } ?>
							<button data-filter="" data-text="All" class="option all is-checked">View All</button>
							<?php wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Faculty Filter', 'bonestheme' ),
								'menu_class' => 'faculty-filter',
								'theme_location' => 'faculty-filter',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '%3$s',
								'walker' => new Filter_Walker
							)); ?>
						</div>
					</div>
					<?php wp_nav_menu( array(
						'menu' => 'Faculty Filter',
						'items_wrap' => '<form class="dropdown-filter"><select><option value="*">View All</option>%3$s</select></form>',
						'walker' => new Dropdown_Walker()
					)); ?>
					<?php } 
				} ?>
				</header>
				<div class="people-list">
					<ul class="<?php echo $people_cat ?>">
                    <?php if($people_cat == "alumni"){ ?>
                        <?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'class_year', 'order' => 'DESC')); ?>		   
                    <?php } else { ?>
                        <?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
                        <?php } ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item<?php $areas = get_field('area_of_study'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php $languages = get_field('language_of_study'); if( $languages ): foreach( $languages as $language ): ?> <?php echo $language->slug; ?><?php endforeach; endif;?> <?php if ( $person_link == 'yes' ) { ?>hover<?php } ?>">
							<?php if ( $person_link == 'yes' ) { ?>
                            <?php if(get_field('personal_website')) { ?>
                                <a href="<?php the_field('personal_website'); ?>">
                            <?php } else { ?>
                                <a href="<?php the_permalink() ?>">
                            <?php } ?>
							<?php } ?>
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use UCLA LOGO
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-square-logo-500.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php // If this pages uses email and links to pages
								if ( $person_link == 'yes' && $email == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<?php // If this pages uses email and links to pages
										if ( $person_link == 'yes' && $email == 'yes' ) { ?>
                                            <?php if(get_field('personal_website')) { ?>
                                                <a href="<?php the_field('personal_website'); ?>">
                                            <?php } else { ?>
                                                <a href="<?php the_permalink() ?>">
                                            <?php } ?>
										<?php } ?><?php the_title(); ?><?php if ( $person_link == 'yes' && $email == 'yes' ) { ?>
										</a>
										<?php } ?>
									</dt>
                                    <?php 
                                    if(get_field('class_year')){ ?>
                                        <dd class="class_year">
                                            <?php 
                                            if ( get_field('class_year') ) { ?>
                                                Class of <?php the_field('class_year'); ?>
                                            <?php } ?>
                                            <?php 
                                            if ( get_field('other_class_year') ) { ?>
                                                / <?php the_field('other_class_year'); ?>
                                            <?php } ?>
                                        </dd>
                                    <?php } ?>
                                    <?php 
                                    if($people_cat == "alumni"){ ?>
                                        <dd class="brief_descript">
                                            <p>
                                                <?php $content = get_the_content();
                                                    $limit = '50';

                                                    if ($person_link == 'yes'){
                                                    $trimmed_content = wp_trim_words( $content, $limit, '...' );
                                                    echo $trimmed_content; 
                                                    } else { 
                                                        the_content();
                                                    }
                                                ?>                      
                                            </p>
                                                <?php if ( $person_link == 'yes' ) { ?>
                                                    <a class="btn" href="<?php the_permalink() ?>">Read More</a>
                                                <?php } ?>  
                                        </dd>
                                    <?php } ?>
									<?php 
									if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
									}
								    if((get_field('department')) && !($people_cat == "alumni")) { ?>
								        <dd class="interest"><?php the_field('department'); ?></dd>
								    <?php }
									if ( $phone == 'yes' ) {
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php the_field('phone_number'); ?></dd>
										<?php } 
									}
									if ( $email == 'yes' ) { 
										if(get_field('email_address')) {
											$person_email = antispambot(get_field('email_address')); ?>
										<dd class="email">
											<a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a>
										</dd>
										<?php }
									}
									if ( $office == 'yes' ) {
										if(get_field('office')) { ?>
										<dd class="office"><?php the_field('office'); ?></dd>
										<?php } 
									}
									if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php the_field('interest'); ?></dd>
										<?php }
									} 
									?>
								</dl>
							<?php if ( $person_link == 'yes' ) { ?>
							</a>
							<?php } ?>
						</li>
					<?php endwhile; ?>
					</ul>
				</div>
			</div>
<?php get_footer(); ?>