<?php
/*
 Template Name: Organization Listing
*/
?>
<?php get_header(); ?>
			<div class="content">
				<div class="col" id="main-content" role="main">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section class="org-list">
							<?php the_content(); ?>
                                <?php if(have_rows('org_list')): ?>
                                <?php while(have_rows('org_list')): the_row(); ?>

                                    <?php
                                        $title = get_sub_field('title');
                                        $description = get_sub_field('description');
                                        $website = get_sub_field('website');
                                        $full_story= get_sub_field('full_story');
                                    ?>
                                <article>						
                                    <section class="entry-content cf">

                                        <?php //the_post_thumbnail( 'people-large', array('class'=>'alignleft') );// ?>
                                        
								<?php // if there is a photo, use it
								if(get_sub_field('image')) {
									$image = get_sub_field('image');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$img_title = $image['title'];
										// thumbnail
										$size = 'people-large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
                                    <?php if($website): ?><a href="<?php echo $website; ?>" ><?php endif; ?>
                                        <img src="<?php echo $thumb; ?>" alt="A photo of <?php echo $img_title; ?>" class="thumb alignleft wp-post-image <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/><?php if($website): ?></a><?php endif; ?>
									<?php } ?>
                                        <?php if($thumb){ ?><div class="brief"><?php } ?>
                                        <h3 class="entry-title">
                                            <a href="<?php echo $website; ?>" rel="bookmark">
                                                <?php echo $title; ?>
                                            </a>
                                        </h3>
                                        
                                        <p>
                                        <?php $content = $description;
                                            $limit = '30';

                                            $trimmed_content = wp_trim_words( $content, $limit, '...' );
                                           // echo $trimmed_content; 
                                            echo $content;
                                        ?>                            
                                        </p>
                                            <?php if($website): ?><a href="<?php echo $website; ?>"  class="btn">Website</a><?php endif; ?>
                                        <?php if($thumb){ ?></div><?php } ?>
                                    </section>
                                </article>
                                <?php endwhile; ?>
                                <?php endif; ?>				
						</section>
					</article>
                    
                    
                    
                    
                    
                    

					

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>