<?php
/*
 Template Name: Courses List Template
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						$program = get_field('program');
						$term = get_field('quarter');
						$qt = $term->name;
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
										
					<?php // Anthropology
					$anthropology_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array( 'relation' => 'AND',
							array(
								'key' => 'program',
								'value' => $program,
							),
							array(
								'key' => 'course_type',
								'value' => 'anthropology',
							))
						));
					?>
					<h2 id="anthropology"><?php echo $qt; ?>: Anthropology Courses</h2>
					<?php if ( $anthropology_loop->have_posts() ) : while ( $anthropology_loop->have_posts() ) : $anthropology_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $anthropology_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $anthropology_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in anthropology this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					
					<?php // Art History
					$arthistory_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' => 
						array( 'relation' => 'AND',
							array(
								'key' => 'program',
								'value' => $program,
							),
							array(
								'key' => 'course_type',
								'value' => 'art-history',
							))
						));
					?>
					<h2 id="arthistory"><?php echo $qt; ?>: Art History Courses</h2>
					<?php if ( $arthistory_loop->have_posts() ) : while ( $arthistory_loop->have_posts() ) : $arthistory_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $arthistory_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $arthistory_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in art history this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					
					<?php // Classics
					$classics_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array( 'relation' => 'AND',
							array(
								'key' => 'program',
								'value' => $program,
							),
							array(
								'key' => 'course_type',
								'value' => 'classics',
							))
						));
					?>
					<h2 id="classics"><?php echo $qt; ?>: Classics Courses</h2>
					<?php if ( $classics_loop->have_posts() ) : while ( $classics_loop->have_posts() ) : $classics_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $classics_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<div><strong>Instructor: </strong><?php the_field('additional_instructors'); ?></div>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $classics_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in classics this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
					
					<?php // History
					$history_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array( 'relation' => 'AND',
							array(
								'key' => 'program',
								'value' => $program,
							),
							array(
								'key' => 'course_type',
								'value' => 'history',
							))
						));
					?>
					<h2 id="history"><?php echo $qt; ?>: History Courses</h2>
					<?php if ( $history_loop->have_posts() ) : while ( $history_loop->have_posts() ) : $history_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $history_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $history_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in history this quarter.</p>
					<?php endif; ?>
                    <?php wp_reset_postdata(); ?>
					
					<?php // Philosophy
					$philosophy_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'course_number', 'order' => 'ASC', 'meta_query' =>
						array( 'relation' => 'AND',
							array(
								'key' => 'program',
								'value' => $program,
							),
							array(
								'key' => 'course_type',
								'value' => 'philosophy',
							))
						));
					?>
					<h2 id="philosophy"><?php echo $qt; ?>: Philosophy Courses</h2>
					<?php if ( $philosophy_loop->have_posts() ) : while ( $philosophy_loop->have_posts() ) : $philosophy_loop->the_post(); ?>
					<div class="course">
						<h4><?php the_title(); ?></h4>
						<?php if(get_field('instructor_type') == "internal") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<?php $philosophy_loop->reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "external") { ?>
						<span class="instructors">
							<?php if(get_field('additional_instructors')) { ?>
							<div><strong>Instructor: </strong><?php the_field('additional_instructors'); ?></div>
							<?php } ?>
						</span>
						<?php }	?>
						<?php if(get_field('instructor_type') == "both") { ?>
						<span class="instructors">
							<strong>Instructor: </strong>
							<?php $instructor = get_field('instructor'); ?>
							<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $philosophy_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
						</span>
						<?php }	?>
						<?php the_content(); ?>
					</div>
					<?php endwhile; else : ?>
					<p>There are no courses in classics this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>
				<?php // get_sidebar(); ?>
                <div class="col side">
					<div class="content col-nav">
						<nav role="navigation" aria-labelledby="person navigation">							
                            <ul class="table-of-contents">
                                <h3>Table of Contents</h3>
                                <li><a href="#anthropology">Anthropology</a></li>
                                <li><a href="#arthistory">Arthistory</a></li>
                                <li><a href="#classics">Classics</a></li>
                                <li><a href="#history">History</a></li>
                                <li><a href="#philosophy">Philosophy</a></li>
                            </ul>
						</nav>
					</div>
                </div>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>