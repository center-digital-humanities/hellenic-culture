<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
			<div id="main-content" role="main">
				<?php // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image'); // get all the rows
					$rand_row = $rows[ array_rand($rows) ]; // get a random row
					$silder_image = $rand_row['image']; // get the sub field value
					$slider_title = $rand_row['title']; // get the sub field value 
					$slider_description = $rand_row['description']; // get the sub field value 
					$slider_button = $rand_row['button_text']; // get the sub field value 
					$slider_link = $rand_row['link']; // get the sub field value 			
					if(!empty($silder_image)): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if($silder_image): ?>
				<?php if($slider_link): ?>
				<a href="<?php echo $slider_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
						<div class="content <?php if($slider_title || $slider_description): ?>text<?php endif; ?>">
						<?php if($slider_title || $slider_description): ?>
							<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
								<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
									<?php if($slider_title): ?>
									<h2><?php echo $slider_title; ?></h2>
									<?php endif; ?>
									<?php if($slider_description): ?>
									<p><?php echo $slider_description; ?></p>
									<?php endif; ?>
									<?php if($slider_button): ?>
									<button class="outline"><?php echo $slider_button; ?></button>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				<?php if($slider_link): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to Slider
				if(get_field('hero_type', 'option') == "slider") { ?>
				<script type="text/javascript">
					jQuery("document").ready(function($) {
						$(document).ready(function(){
						  $('#bxslider').bxSlider({
						  	autoHover: true,
						  	auto: false,
						  });
						});
					});
				</script>
				<div id="slider">
					<ul id="bxslider">
						<?php if(have_rows('hero_image')): ?>
						<?php while(have_rows('hero_image')): the_row(); ?>
						<?php
							$slider_title = get_sub_field('title');
							$slider_description = get_sub_field('description');
							$slider_link = get_sub_field('link');
							$silder_image = get_sub_field('image');
							$slider_button = get_sub_field('button_text');
							if(!empty($silder_image)): 
								// vars
								$url = $silder_image['url'];
								$title = $silder_image['title'];
								// thumbnail
								$size = 'home-hero';
								$slide = $silder_image['sizes'][ $size ];
								$width = $silder_image['sizes'][ $size . '-width' ];
								$height = $silder_image['sizes'][ $size . '-height' ];
							endif;
						?>		
						<?php if($slider_link): ?>
						<a href="<?php echo $slider_link; ?>" class="hero-link">
						<?php endif; ?>
							<li style="background-image: url('<?php echo $slide; ?>');">
								<div class="bg">
									<div class="content">
										<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
											<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
												<?php if($slider_title): ?>
												<h2><?php echo $slider_title; ?></h2>
												<?php endif; ?>
												<?php if($slider_description): ?>
												<p><?php echo $slider_description; ?></p>
												<?php endif; ?>
												<?php if($slider_button): ?>
												<button class="outline"><?php echo $slider_button; ?></button>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php if($slider_link): ?>
						</a>
						<?php endif; ?>
						<?php endwhile; ?>
						<?php endif; ?>
					</ul>
				</div>
				<?php } ?>
				<div class="content">
					<?php
					if(have_rows('homepage_columns')) :
						while (have_rows('homepage_columns')) : the_row();
							
							// For showing snippet from any page
							if(get_row_layout() == 'page_excerpt') 
								get_template_part('snippets/col', 'page');
					        
							// For showing list of recent post
							elseif(get_row_layout() == 'recent_posts') 
								get_template_part('snippets/col', 'posts');
							
							// For showing free form content
							elseif(get_row_layout() == 'content_block') 
								get_template_part('snippets/col', 'content');
							
							// For showing list of events from event widget
							elseif(get_row_layout() == 'upcoming_events') 
					       		get_template_part('snippets/col', 'events');
					       	
					       	// For showing a menu
					       	elseif(get_row_layout() == 'menu') 
					       		get_template_part('snippets/col', 'menu');
					       	
					       	// For showing a widget
				       		elseif( get_row_layout() == 'widget_1' ) 
				       			get_template_part('snippets/col', 'widget-1');
							
						endwhile;
					endif;
			    	?>
				</div>
                <div class="support">
                    <div class="content">
                        <div class="suppport_txt">
                            
                        <?php if(have_rows('homepage_rows')) :
                            while(have_rows('homepage_rows')) : the_row();
                            
                            //if(get_row_layout() == 'support_rows'){
                                
                                $support = get_sub_field('support_content');
                                $graph = get_sub_field('support_graph');
                               // $s_page = get_sub_field('page');
                               // $s_content = get_sub_field('content');
                               // $s_btn_txt = get_sub_field('button_text');
                               // $s_btn_link = get_sub_field('button_link');

                            if($support){
                        ?>
                            <?php if($support['support_title']){ ?>
                                <h3><?php echo $support['support_title']; ?></h3>
                            <?php } ?>
                        <?php if( ($support['text_source'] == 'content_block' )){ ?>
                            <?php if($support['content']){ ?>
                                <p><?php echo $support['content']; ?></p>
                            <?php }  ?>
                        <?php } else { ?>
                            <?php if($support['page']){ ?>
                        

	<?php $page = $support['page'];
		$page_query = new WP_Query( array( 'page_id' => $page ) );
		if ($page_query->have_posts()) : while ($page_query->have_posts()) : $page_query->the_post(); ?>
	<p>
		<?php $content = get_the_content($page);
			//$limit = get_sub_field('word_limit');
            $limit = 50;
                                                       
			
		    $trimmed_content = wp_trim_words( $content, $limit, '...' );
		    echo $trimmed_content; 
		?>
	   <a class="btn" href="<?php the_permalink() ?>">Read More</a>
	</p>
	<?php endwhile; endif; ?>
	<?php wp_reset_postdata(); ?>

                        
                            <?php } } ?>
                            <div class="donate-btn">
                                <a class="btn" href="<?php echo $support['button_link']; ?>"><?php echo $support['button_text']; ?> <span>click here</span></a>
                                <span class="fas fa-heart" aria-hidden="true"></span>
                            </div>
                            <?php } ?>
                                <?php // Graph
                                    if($graph){ ?>
                                        <div class="percent-txt">
                                            <?php if($graph['goal_percent']){ ?>
                                                <h1><?php echo $graph['goal_percent']; ?><span>%</span></h1>
                                            <?php } ?>

                                            <?php if($graph['tagline']){ ?>
                                                <h2><?php echo $graph['tagline']; ?></h2>
                                            <?php } ?>
                                        </div>
                        </div> 
                                        <?php if($graph['goal_percent']){ ?>
                                        <?php
                                                 //$graph['goal_percent'];
                                                //$percent = 0.3;
                                                $size = 320;
                                                //$total = ( $percent * $size );
                                               // echo $total;

                                            function getPercentOfNumber($number, $percent){
                                                    return ($percent / 100) * $number;
                                                }

                                               // echo getPercentOfNumber(320, 20);
                                                $percent = ($size - getPercentOfNumber($size, $graph['goal_percent']) );
                                            ?>
                                            <div class="graphic">
                                                <div class="bar-container" style="float: right;">
                                                    <div class="p_line">
                                                         <img class="g_line" src="<?php echo get_template_directory_uri(); ?>/library/images/graph_line.png" alt="line"/>
                                                         <span class="fas fa-caret-left" style=" bottom: <?php echo $graph['goal_percent']; ?>%;" aria-hidden="true"></span> 
                                                    </div>
                                                     <div class="goal-bar2">
                                                        <div class="bar-wrap">
                                                           
                                                            <div class="bar" style="clip: rect(<?php echo $percent; ?>px, 450px, 320px, 0px);"><img class="pb_fill" src="<?php echo get_template_directory_uri(); ?>/library/images/parthenon-building-colored.png" alt="parthenon Graphic"/></div>
                                                           <!--// <div class="bar" style="transform: translateY(<?php echo $graph['goal_percent']; ?>%);"><img class="pb_fill" src="<?php echo get_template_directory_uri(); ?>/library/images/parthenon-building-colored.png" alt="parthenon Graphic"/></div> //-->
                                                         </div>
                                                            <img class="pb_bg" src="<?php echo get_template_directory_uri(); ?>/library/images/parthenon-building-bg.png" alt="parthenon Graphic"/>
                                                     </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        
                                    
                                   <?php } ?>
                        
                               <?php // } 
                                endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="donors">
                    <div class="donors-col <?php the_field('donors_post_width'); ?>">
                        <div class="content">
                        <h3>Donors</h3>
                       <?php if(get_field('donors_title')){?> <h3><?php the_field('donors_title'); ?></h3><? }php?>
                        <?php $term = get_sub_field('donors_category');
                            $amount = get_field('donors_amount_to_show');
                            $posts_query = new WP_Query( array( 'post_type' => 'donors', 'showposts' => $amount, 'posts_per_page' => 1, 'cat' => $term->term_id, 'orderby' => 'rand' ) ); ?>
                        <ul <?php if(get_field('donors_show_categories') == "yes") { ?> class="categories" <?php } ?>>
                            <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
                            <?php if(get_field('donors_show_categories') == "no") { ?><a href="<?php the_permalink() ?>"><?php } ?>
                                <li>
                                    <?php  

                                       // if(get_field('donors_post_width') == "two") { 
                                          //  if(get_field('donors_show_image') == "yes") { 
                                                if(get_field('donors_show_categories') == "yes") { ?>
                                                    <a href="<?php the_permalink() ?>">
                                                <?php }
                                                    if ( has_post_thumbnail() ) {
                                                        $dthumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'people-large' );
                                                        $durl = $dthumb['0']; ?>
                                                       <img src="<?=$durl?>" alt="A photo of <?php the_title(); ?>" class="mobile" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" class="mobile" />
                                                <?php }
                                                if(get_field('donors_show_categories') == "yes") { ?>
                                                    </a>
                                                <?php }
                                           // }
                                        //} 
                                    ?>
                                                                                                    
                                    <h4><?php the_title(); ?></h4>
                                    <div class="item">
                                    <?php if(get_field('donors_show_categories') == "yes") { ?>
                                        <span class="category"><?php the_category( ', ' ); ?></span>
                                    <?php } ?>
                                        <?php if(get_field('donors_show_categories') == "yes") { ?><a href="<?php the_permalink() ?>"><?php } ?>
                                        <p>
                                            <?php $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, 45, '... ' );
                                            echo $trimmed_content; ?>
                                            <a href="<?php the_permalink() ?>" class="link">Read more</a> 
                                        </p>
                                        
                                        <!-- a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a /-->
                                        <?php if(get_field('donors_show_categories') == "yes") { ?></a><?php } ?>
                                    </div>
                                </li>
                            <?php if(get_field('donors_show_categories') == "no") { ?></a><?php } ?>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                        
                        <div class="view-btn"><?php $terms_student = get_field('donors_category'); ?>
                            <?php if( $terms_student ) { ?>
                            <a class="btn" href="/category/<?php echo $terms_student->slug; ?>/">View All<span class="hidden"> <?php echo $terms_student->name; ?></span></a>
                            <?php } else { ?>
                            <a class="btn" href="/donors/">View All<span class="hidden">Donors</span></a>
                            <?php } ?>                    
                        </div>
                        </div>
                    </div>                 
                    
                    <div id="hero" class="desktop halfCircleRight" style="background-image: url('<?=$durl?>');"></div>
                </div>					
			</div>
		</div>
<?php get_footer(); ?>