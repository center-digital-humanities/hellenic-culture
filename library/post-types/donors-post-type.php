<?php
// Donors Post Type Settings

// add custom categories
register_taxonomy( 'donor_cat', 
	array('donors'), /* if you change the name of register_post_type( 'donors', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Donor Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Donor Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Donor Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Donor Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Donor Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Donor Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Donor Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Donor Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Donor Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Donor Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'donors_cat' )
	)
);

// let's create the function for the custom type
function donors_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'donors', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Donors', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Donor', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Donors', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Donor', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Donor', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Donor', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Donor', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Donors', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No Donors added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all donors', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 6, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-businessman', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'donor', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'donors', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
            'supports' => array( 'title', 'editor', 'revisions', 'author', 'page-attributes', 'thumbnail', 'excerpt' )
		) /* end of options */
	); /* end of register post type */	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'donors_post_type');
?>