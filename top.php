<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>" rel="nofollow" class="logos">
				<div class="dept-logo">             
				    <?php // Custom Logo code start
					if (has_custom_logo()) {  // Display the Custom Logo
						the_custom_logo();
					} else {  // No Custom Logo, just display the site's name ?>
						<h1 class="logo-text" alt="<?php the_field('department_name', 'option'); ?>"><?php bloginfo('name'); ?></h1>
					<?php } // Custom Logo code ends ?>
				</div>
			</a>
		</div>
		<nav role="navigation" aria-labelledby="main navigation" class="desktop">
			<?php wp_nav_menu(array(
				'container' => false,
				'menu' => __( 'Main Menu', 'bonestheme' ),
				'menu_class' => 'main-nav',
				'theme_location' => 'main-nav',
				'before' => '',
				'after' => '',
				'depth' => 2,
			)); ?>
		</nav>
        <?php if(get_field('enable_donation', 'option') == "enable") { ?>
            <div class="give-back">
                <?php if(get_field('link_type', 'option') == "internal") { ?>
                <a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
                <?php }?>
                <?php if(get_field('link_type', 'option') == "external") { ?>
                <a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
                <?php }?><span class="fas fa-heart" aria-hidden="true"></span>
                <?php the_field('button_text', 'option'); ?></a>
                <?php if(get_field('supporting_text', 'option')) { ?>
                <span class="support"><?php the_field('supporting_text', 'option'); ?></span>
                <?php }?>
            </div>
        <?php }?>
		<?php //get_search_form(); ?>
	</header>
        <?php
            //Donors Here for Archive
            if(is_post_type_archive('donors')){
            
        ?>
                <div class="featured-donors">
                    <div class="donors-col <?php the_field('donors_post_width'); ?>">
                        <div class="content">
                        <h2>Featured Donor</h2>
                       <?php if(get_field('donors_title')){?> <h3><?php the_field('donors_title'); ?></h3><? }php?>
                        <?php $term = get_sub_field('donors_category');
                            $amount = get_field('donors_amount_to_show');
                            $posts_query = new WP_Query( array( 'post_type' => 'donors', 'showposts' => $amount, 'posts_per_page' => 1, 'cat' => $term->term_id, 'orderby' => 'rand' ) ); ?>
                        <ul <?php if(get_field('donors_show_categories') == "yes") { ?> class="categories" <?php } ?>>
                            <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
                            <?php if(get_field('donors_show_categories') == "no") { ?><a href="<?php the_permalink() ?>"><?php } ?>
                                <li>
                                                                
                                    <h3><?php the_title(); ?></h3>
                                    <?php  

                                       // if(get_field('donors_post_width') == "two") { 
                                          //  if(get_field('donors_show_image') == "yes") { 
                                                if(get_field('donors_show_categories') == "yes") { ?>
                                                    <a href="<?php the_permalink() ?>">
                                                <?php }
                                                    if ( has_post_thumbnail() ) {
                                                        $dthumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'people-large' );
                                                        $durl = $dthumb['0']; ?>
                                                       <img src="<?=$durl?>" alt="A photo of <?php the_title(); ?>" class="mobile" />
                                                    <?php } else { ?>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" class="mobile" />
                                                <?php }
                                                if(get_field('donors_show_categories') == "yes") { ?>
                                                    </a>
                                                <?php }
                                           // }
                                        //} 
                                    ?>
                                    <div class="item">
                                    <?php if(get_field('donors_show_categories') == "yes") { ?>
                                        <span class="category"><?php the_category( ', ' ); ?></span>
                                    <?php } ?>
                                        <?php if(get_field('donors_show_categories') == "yes") { ?><a href="<?php the_permalink() ?>"><?php } ?>
                                        <p>
                                            <?php $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, 50, '... ' );
                                            echo $trimmed_content; ?>
                                        </p>
                                        <?php if(get_field('donors_show_categories') == "yes") { ?></a><?php } ?>
                                    </div>                                        
                                    <div class="view-btn">
                                        <a class="btn" href="<?php the_permalink(); ?>">Read More</a>
                                    </div>
                                </li>
                            <?php if(get_field('donors_show_categories') == "no") { ?></a><?php } ?>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                        </div>
                    </div>                 
                    
                    <div id="hero" class="desktop halfCircleRight" style="background-image: url('<?=$durl?>');"></div>
                </div>
        <?php } ?>
        
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category( $category ) || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
            // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image'); // get all the rows
					$rand_row = $rows[ array_rand($rows) ]; // get a random row
					$silder_image = $rand_row['image']; // get the sub field value
					$slider_title = $rand_row['title']; // get the sub field value 
					$slider_description = $rand_row['description']; // get the sub field value 
					$slider_button = $rand_row['button_text']; // get the sub field value 
					$slider_link = $rand_row['link']; // get the sub field value 			
					if(!empty($silder_image)): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if($silder_image): ?>
				<?php if($slider_link): ?>
				<a href="<?php echo $slider_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
						<div class="content <?php if($slider_title || $slider_description): ?>text<?php endif; ?>">
						<?php if($slider_title || $slider_description): ?>
							<div class="hero-description <?php the_field('vertical_text_alignment', 'option'); ?>">
								<div class="content <?php the_field('horizontal_text_alignment', 'option'); ?>">
									<?php if($slider_title): ?>
									<h2><?php echo $slider_title; ?></h2>
									<?php endif; ?>
									<?php if($slider_description): ?>
									<p><?php echo $slider_description; ?></p>
									<?php endif; ?>
									<?php if($slider_button): ?>
									<button class="outline"><?php echo $slider_button; ?></button>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				<?php if($slider_link): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } 
            
            
            
            
            
            
            
            
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>