<?php get_header(); ?>
<?php
    //$term = get_sub_field('category');
    //$amount = get_sub_field('amount_to_show');
    $term = 'interviews';
    $amount = 10;

// find date time now
$date_now = date('Y-m-d H:i:s');
$time_now = strtotime($date_now);


// find date time in 7 days
$time_next_week = strtotime('+12 month', $time_now);
$date_next_week = date('Y-m-d H:i:s', $time_next_week);

$slug = get_queried_object()->post_name;
//echo $slug;

if(($slug == 'past-events') || ($slug == 'old-stuff') || ($slug == 'pevents')){

// Upcoming Events Query (https://www.advancedcustomfields.com/resources/date-time-picker/)
// query events
$args = array(
	'posts_per_page'	=> -1,
        //'donor_cat' => 'featured, hcps, career-panel-series, workshops', 
       'post_type' => 'donors', 
	'meta_query' 		=> array(
		array(
	        'key'			=> 'date_of_event',
	        'compare'		=> 'BETWEEN',
	        'value'			=> array( $date_now, $date_next_week ),
	        'type'			=> 'DATETIME'
	    )
    ),
	'order'				=> 'ASC',
	'orderby'			=> 'meta_value',
	'meta_key'			=> 'date_of_event',
	'meta_type'			=> 'DATETIME'
);
}else{
    

//Return posts from January 1st to February 28th  Source (https://codex.wordpress.org/Class_Reference/WP_Query)
//PAST EVENTS
    
    $past = 'January 1st, 2010';
    
    $args = array(
	'posts_per_page'	=> -1,
    //'donor_cat' => 'events, hcps', 
    'post_type' => 'donors', 
	'meta_query' 		=> array(
		array(
	        'key'			=> 'date_of_event',
	        'compare'		=> 'BETWEEN',
	        'value'			=> array( $past, $date_now ),
	        'type'			=> 'DATETIME'
	    )
    ),
	'order'				=> 'DESC',
	'orderby'			=> 'meta_value',
	'meta_key'			=> 'date_of_event',
	'meta_type'			=> 'DATETIME'
);
    
    
}
   $posts_query = new WP_Query( $args );

   // 'date_query' => array( array( 'after' => '3 days ago' ));

?> 



			<div class="content main">
				<div class="col" id="main-content" role="main">
					<h1 class="archive-title">
						Donors
					</h1>
        <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						
						<section class="entry-content cf">
                            <a href="<?php the_permalink() ?>" ><?php the_post_thumbnail( 'people-large', array('class'=>'alignleft') ); ?></a>
                            <?php if ( has_post_thumbnail() ) { ?><div class="brief"><?php } ?>
                                <h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                <p>
                                <?php $content = get_the_content();
                                    $limit = '35';

                                    $trimmed_content = wp_trim_words( $content, $limit, '...' );
                                    echo $trimmed_content; 
                                ?>                            
                                </p>
                                <a href="<?php the_permalink() ?>" class="btn">Read More</a>
                            <?php if ( has_post_thumbnail() ) { ?></div><?php } ?>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section>
							<p>There is nothing available to show here at this time. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
                <!--//
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
                                // If an Study Abroad subpage								
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Donors', 'bonestheme' ),
									   	'menu_class' => 'donors-nav',
									   	'theme_location' => 'donors-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Donors</h3> <ul>%3$s</ul>'
									));
							?>
						</nav>
					</div>
				</div>
                //-->
			</div>

<?php get_footer(); ?>